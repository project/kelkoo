Kelkoo Feed
Version: 	1.1
Author:		Parameshwar Babu
e-Mail:		info@paramprojects.com / netminster@gmail.com
Website:	http://paramprojects.com


Compatability:
Drupal 		6.1
Ubercart 	2.x
XML:		1.0


------------------------------------------------------------------------------------------------------
QUICK INFO
==========
To access the kelkoo feed, you just need to access: http://www.yoursite.com/products/kelkoo.xml

PURPOSE
=======
Kelkoo is a leading shopping site in UK/Europe and currently taken over by Yahoo. If you are running an ecommerce/shopping
site in Ubercart, you will be tempted to publish your products to Kelkoo store. However, it takes a lot of time to find
get proper documentation from Kelkoo staff for publishing/uploading your products. We have undergone this process and
hence I hope this module help a lot of people in the drupal community.


STEP BY STEP QUIDE
==================

1) Upload the module into sites/all/modules folder of your drupal installation.
2) Enable the module. Go to admin/store/settings/kelkoo and you may configure
   the title and description for your product feed.
3) Now, you can access your feed from here http://www.yoursite.com/products/kelkoo.xml   
   and then submit it to your kelkoo store.
   
NOTE:
====
Before generating the feed, it checks if each product is "in stock". "Out of stock" products will not be published.   


Contact
-------
I and my team handle a lot of ecommerce/drupal projects from clients around the world - so feel free to contact me
if you need any help as we have enough resources to handle plenty of work.

EMail:		info@paramprojects.com / netminster@gmail.com
Website:	http://paramprojects.com/website

